// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "T4PToyBase.generated.h"

UCLASS()
class TEST4PEOPLE_API AT4PToyBase : public AActor
{
public:
	GENERATED_BODY()


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Mesh, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* ToyMesh;



	// Sets default values for this actor's properties
	AT4PToyBase();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Toy")
	int PointsToAdd;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Toy")
	TSoftObjectPtr<UStaticMesh> ToyMeshPreviewSoft;
	

protected:
	UFUNCTION()
	void SendOnDestroyedEvent(AActor* DestroyedActor);

	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UFUNCTION(BlueprintCallable, Category = "Toy")
	void SendToyDropEvent();

	UFUNCTION(BlueprintCallable, Category = "Toy")
	void SendToyHitEvent();
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
