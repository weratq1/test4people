// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "T4PToyBase.h"
#include "GameFramework/GameModeBase.h"
#include "T4PGameMode.generated.h"

class AT4PAIBotBase;


UCLASS(minimalapi)
class AT4PGameMode : public AGameModeBase
{

	
private:
	GENERATED_BODY()

public:
	AT4PGameMode();
	
	UFUNCTION(Blueprintable)
	void RegisterAI(AT4PAIBotBase* Bot);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	float DistancePercentToIgnoreToy;
	
	UPROPERTY(BlueprintReadWrite)
	AT4PToyBase* CurrentToy;
protected:
	UPROPERTY(BlueprintReadWrite)
	TArray<AT4PAIBotBase*> RegisteredBots;
	
	UFUNCTION(BlueprintCallable)
	void ToyUnreachable();
	
	UPROPERTY()
    FTimerHandle FinialCheckTimerHandle;

	UFUNCTION()
	void OnToyDrop(AT4PToyBase* Toy);
	
	virtual void InitGameState() override;
	
	UFUNCTION(BlueprintCallable)
	void FinalCheckReachToy();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	float FinalCheckTime;
	
	UFUNCTION()
	void CalculateDistanceAndCheck(TMap<AT4PAIBotBase*, float>& BotDistances, float& MaxDistance);
	
	UFUNCTION()
	void ExecuteRunInAI(AT4PToyBase* Toy, TMap<AT4PAIBotBase*, float> BotDistances, float MaxDistance);
};



