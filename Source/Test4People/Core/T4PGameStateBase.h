// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "T4PGameStateBase.generated.h"

/**
 * 
 */
class AT4PToyBase;
class AT4PAIBotBase;

USTRUCT(BlueprintType)
struct FAIStatistic
{
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	AT4PAIBotBase* Bot;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString Name;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 Points;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FToyDropDelegate, AT4PToyBase*, Toy);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCanDropToyDelegate, bool, bCanDropToy);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FMatchEndDelegate);


UCLASS()
class TEST4PEOPLE_API AT4PGameStateBase : public AGameStateBase
{
protected:
	virtual void BeginPlay() override;


private:
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintAssignable, Category = "Toy")
	FToyDropDelegate OnToyHitDelegate;

	UPROPERTY(BlueprintAssignable, Category = "Toy")
	FCanDropToyDelegate OnCanDropToy;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, ReplicatedUsing=OnRep_bCanDropToy, Category = "Toy")
	bool bCanDropToy;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, ReplicatedUsing=OnRep_BotsStat ,Category = "AI")
	TArray<FAIStatistic> BotsStat;

	UFUNCTION(BlueprintNativeEvent)
	void OnRep_BotsStat();

	UFUNCTION(BlueprintCallable)
	void SetCanDropToy(bool bCanDrop);

	UFUNCTION()
	void OnRep_bCanDropToy();

	// Match time in seconds 
	UPROPERTY(BlueprintReadWrite, EditAnywhere, ReplicatedUsing=OnRep_MatchTime, Category = "Match")
	int32 MatchTime;

	UFUNCTION(BlueprintNativeEvent)
	void OnRep_MatchTime();

	UFUNCTION(BlueprintCallable, Reliable, NetMulticast, Category = "Match")
	void OnMatchEnd();
	
	UPROPERTY(BlueprintAssignable, Category = "Match")
	FMatchEndDelegate OnMatchEndDelegate;
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(BlueprintCallable)
	void RegisterAI(AT4PAIBotBase* Bot);
	
    UFUNCTION(BlueprintNativeEvent)
    void OnToyHit(AT4PToyBase* Toy);

    UFUNCTION(BlueprintNativeEvent)
    void OnToyDrop(AT4PToyBase* Toy);
protected:
	UFUNCTION()
	void TickMatchTime();
	
	UFUNCTION(BlueprintCallable)
	void RefreshAIStatistics();
	
	UPROPERTY()
    FTimerHandle MatchTimeHandle;

    UFUNCTION()
    void OnToyDestroyed(AActor* DestroyedActor);
};
