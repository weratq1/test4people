// Copyright Epic Games, Inc. All Rights Reserved.

#include "T4PGameMode.h"

#include "T4PGameStateBase.h"
#include "AI/T4PAIBotBase.h"
#include "Kismet/KismetMathLibrary.h"
#include "UObject/ConstructorHelpers.h"





AT4PGameMode::AT4PGameMode()
	: Super()
{
	DistancePercentToIgnoreToy = 0.8f;
}

void AT4PGameMode::RegisterAI(AT4PAIBotBase* Bot)
{
	if(IsValid(Bot))
	{
		RegisteredBots.Add(Bot);
	}
}

void AT4PGameMode::ToyUnreachable()
{	
	if(IsValid(CurrentToy))
	{
		CurrentToy->Destroy();
	}
}


void AT4PGameMode::InitGameState()
{
	Super::InitGameState();
	AT4PGameStateBase* GS = GetGameState<AT4PGameStateBase>();
	if(GS)
	{
		GS->OnToyHitDelegate.AddDynamic(this, &AT4PGameMode::OnToyDrop);
	}
}



void AT4PGameMode::CalculateDistanceAndCheck(TMap<AT4PAIBotBase*, float>& BotDistances, float& MaxDistance)
{
	MaxDistance = 0.f;
	for(AT4PAIBotBase* Bot : RegisteredBots)
	{
		float Distance = Bot->CalculateTimeToReachLocation(CurrentToy->GetActorLocation());
		if(Distance > MaxDistance)
		{
			MaxDistance = Distance;
		}
		BotDistances.Add(Bot, Distance);
	}
}

void AT4PGameMode::ExecuteRunInAI(AT4PToyBase* Toy, TMap<AT4PAIBotBase*, float> BotDistances, float MaxDistance)
{
	BotDistances.ValueSort([](const float& A, const float& B) { return A < B; });
	float MinDistance = 0.f;
	for(auto& Elem : BotDistances)
	{
		if(MinDistance == 0.f)
		{
			Elem.Key->StartMovementToActor(Toy);
			MinDistance = Elem.Value;
		}
		else
		{
			if(UKismetMathLibrary::NormalizeToRange(Elem.Value, MinDistance, MaxDistance) < DistancePercentToIgnoreToy)
			{
				Elem.Key->StartMovementToActor(Toy);
			}
		}
	}
}

void AT4PGameMode::OnToyDrop(AT4PToyBase* Toy)
{
	if(!IsValid(Toy))
	{
		return;
	}
	CurrentToy = Toy;
	TMap<AT4PAIBotBase*, float> BotDistances;
	float MaxDistance;
	CalculateDistanceAndCheck(BotDistances, MaxDistance);
	//if no bots can reach the toy, wait for the final check
	if(MaxDistance == 0.f)
	{
		GetWorld()->GetTimerManager().SetTimer(FinialCheckTimerHandle, this, &AT4PGameMode::FinalCheckReachToy, FinalCheckTime, false);
		return;
	}
	ExecuteRunInAI(Toy, BotDistances, MaxDistance);
}


void AT4PGameMode::FinalCheckReachToy()
{
	if(IsValid(CurrentToy))
	{
		TMap<AT4PAIBotBase*, float> BotDistances;
		float MaxDistance;
		CalculateDistanceAndCheck(BotDistances, MaxDistance);
		if(MaxDistance == 0.f)
		{
			//no bot can reach the toy
			ToyUnreachable();
		}
		else
		{
			ExecuteRunInAI(CurrentToy, BotDistances, MaxDistance);
		}
	}
}