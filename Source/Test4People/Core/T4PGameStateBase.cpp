// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/T4PGameStateBase.h"
#include "T4PToyBase.h"
#include "Net/UnrealNetwork.h"
#include "AI/T4PAIBotBase.h"

void AT4PGameStateBase::OnRep_bCanDropToy()
{
	OnCanDropToy.Broadcast(bCanDropToy);
}

void AT4PGameStateBase::OnRep_MatchTime_Implementation()
{
}

void AT4PGameStateBase::OnToyHit_Implementation(AT4PToyBase* Toy)
{
	if(IsValid(Toy))
	{
     	OnToyHitDelegate.Broadcast(Toy);
	}
}

void AT4PGameStateBase::BeginPlay()
{
	Super::BeginPlay();
	if(GetLocalRole()== ROLE_Authority)
	{
		GetWorldTimerManager().SetTimer(MatchTimeHandle, this, &AT4PGameStateBase::TickMatchTime, 1.f, true);
	}
}

void AT4PGameStateBase::OnToyDrop_Implementation(AT4PToyBase* Toy)
{
	if(IsValid(Toy))
	{
		SetCanDropToy(false);
		Toy->OnDestroyed.AddDynamic(this, &AT4PGameStateBase::OnToyDestroyed);
	}
}

void AT4PGameStateBase::OnToyDestroyed(AActor* DestroyedActor)
{
	SetCanDropToy(true);
	RefreshAIStatistics();
}

void AT4PGameStateBase::RefreshAIStatistics()
{
	for(int i = 0; i < BotsStat.Num(); i++)
	{
		BotsStat[i].Points = BotsStat[i].Bot->CurrPoints;
	}
	BotsStat.Sort([&](const FAIStatistic& A, const FAIStatistic& B)
	{
		return A.Points > B.Points;
	});
	OnRep_BotsStat();
}

void AT4PGameStateBase::RegisterAI(AT4PAIBotBase* Bot)
{
	if(IsValid(Bot) && GetLocalRole() == ROLE_Authority)
	{
		FAIStatistic Stat;
		Stat.Bot = Bot;
		Stat.Name = Bot->GetActorNameOrLabel();
		Stat.Points = 0;
		BotsStat.Add(Stat);
	}
}



void AT4PGameStateBase::OnRep_BotsStat_Implementation()
{
	
}

void AT4PGameStateBase::SetCanDropToy(bool bCanDrop)
{
	bCanDropToy = bCanDrop;
	OnCanDropToy.Broadcast(bCanDrop);
}

void AT4PGameStateBase::TickMatchTime()
{
	MatchTime -= 1.f;
	if(MatchTime <= 0.f)
	{
		MatchTime = 0.f;
		OnMatchEnd();
	}
	OnRep_MatchTime();
}

void AT4PGameStateBase::OnMatchEnd_Implementation()
{
	OnMatchEndDelegate.Broadcast();
	MatchTimeHandle.Invalidate();
}

void AT4PGameStateBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AT4PGameStateBase, bCanDropToy);
	DOREPLIFETIME(AT4PGameStateBase, BotsStat);
	DOREPLIFETIME(AT4PGameStateBase, MatchTime);
}
