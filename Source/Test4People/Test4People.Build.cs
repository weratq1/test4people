// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Test4People : ModuleRules
{
	public Test4People(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "EnhancedInput", "NavigationSystem"});
		PublicIncludePaths.AddRange(
			new string[] {
				"Test4People"
			}
		);
	}
}
