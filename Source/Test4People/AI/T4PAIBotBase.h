// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "GameFramework/Pawn.h"
#include "Core/T4PInteractInterface.h"
#include "T4PAIBotBase.generated.h"

UCLASS()
class TEST4PEOPLE_API AT4PAIBotBase : public APawn, public IT4PInteractInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AT4PAIBotBase();

	UPROPERTY(BlueprintReadWrite, Category = "AI")
	int CurrPoints;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AI")
	float Speed;

	//will use +/- this value to randomize speed
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	float RandomSpeedDeviation;
	
	UFUNCTION(BlueprintCallable, Category = "AI")
	float CalculateTimeToReachLocation(FVector Location);

	UFUNCTION(BlueprintNativeEvent)
	void StartMovementToActor(AActor* Actor);

	UFUNCTION()
	virtual void AddPoints_Implementation(int Points) override;

	UFUNCTION(BlueprintCallable, Category = "AI")
	void RegisterAI();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UFUNCTION(Blueprintable, BlueprintNativeEvent)
	void LookAtPlayer();

	UFUNCTION(BlueprintCallable, Category = "AI")
	void LookForward();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AI")
    UFloatingPawnMovement* MovementComponent;

};
