// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/T4PAIBotBase.h"

#include "NavigationSystem.h"
#include "AI/Navigation/NavigationTypes.h"
#include "Core/T4PGameMode.h"
#include "Core/T4PGameStateBase.h"


// Sets default values
AT4PAIBotBase::AT4PAIBotBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MovementComponent = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("MovementComponent"));

}


float AT4PAIBotBase::CalculateTimeToReachLocation(FVector Location)
{
	FVector StartLocation = GetActorLocation();
	
	FVector::FReal Distance = 0.f;
	UNavigationSystemV1* Nav = Cast<UNavigationSystemV1>( GetWorld()->GetNavigationSystem());
	if(Nav)
	{
		
		ENavigationQueryResult::Type Result;
		Result = Nav->GetPathLength(StartLocation, Location,Distance);
		if(Result == ENavigationQueryResult::Type::Success)
		{
			return MovementComponent->MaxSpeed != 0.f ? Distance / MovementComponent->MaxSpeed : 0.f;
		}
		else
		{
			//try to find a path to the closest point
			FNavLocation ClosestPoint;
			if(Nav->ProjectPointToNavigation(Location, ClosestPoint))
			{
				Result = Nav->GetPathLength(StartLocation, ClosestPoint, Distance);
				if(Result == ENavigationQueryResult::Type::Success)
				{
					return MovementComponent->MaxSpeed != 0.f ? Distance / MovementComponent->MaxSpeed : 0.f;
				}
			}
		}
	}
	return 0.f;
}

void AT4PAIBotBase::StartMovementToActor_Implementation(AActor* Actor)
{
}

void AT4PAIBotBase::LookAtPlayer_Implementation()
{
	if(GetWorld()->GetFirstPlayerController() && GetWorld()->GetFirstPlayerController()->GetPawn())
	{
		SetActorRotation((GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation() - GetActorLocation()).Rotation());
	}
}


void AT4PAIBotBase::LookForward()
{
	SetActorRotation(MovementComponent->Velocity.GetSafeNormal2D().Rotation());
	
}

void AT4PAIBotBase::AddPoints_Implementation(int Points)
{
	IT4PInteractInterface::AddPoints_Implementation(Points);
	CurrPoints += Points;
}

void AT4PAIBotBase::RegisterAI()
{
	if(!GetWorld()){return;}
	
	if(GetWorld()->GetAuthGameMode())
	{
		AT4PGameMode* GM = Cast<AT4PGameMode>(GetWorld()->GetAuthGameMode());
		if(GM)
		{
			GM->RegisterAI(this);
		}
	}
	if(GetWorld()->GetGameState())
	{
		AT4PGameStateBase* GS = Cast<AT4PGameStateBase>(GetWorld()->GetGameState());
		if(GS)
		{
			GS->RegisterAI(this);
		}
	}
}

// Called when the game starts or when spawned
void AT4PAIBotBase::BeginPlay()
{
	Super::BeginPlay();
	MovementComponent->MaxSpeed = FMath::RandRange(FMath::Clamp(Speed - RandomSpeedDeviation,0.f,Speed), Speed + RandomSpeedDeviation);
	if(GetLocalRole() == ROLE_Authority)
	{
		RegisterAI();
	}
}

// Called every frame
void AT4PAIBotBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if(MovementComponent->Velocity.Length() <= 0.1f)
	{
		LookAtPlayer();
	}
	else
	{
		LookForward();
	}
}


