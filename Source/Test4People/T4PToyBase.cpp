// Fill out your copyright notice in the Description page of Project Settings.


#include "T4PToyBase.h"
#include "Core/T4PGameStateBase.h"



// Sets default values
AT4PToyBase::AT4PToyBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ToyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ToyMesh"));
	RootComponent = ToyMesh;
}

void AT4PToyBase::SendOnDestroyedEvent(AActor* DestroyedActor)
{
	if(GetWorld() && GetWorld()->GetGameState())
	{
		AT4PGameStateBase* GS = Cast<AT4PGameStateBase>(GetWorld()->GetGameState());
		if (GS)
		{
			GS->SetCanDropToy(true);
		}
	}
}

// Called when the game starts or when spawned
void AT4PToyBase::BeginPlay()
{
	Super::BeginPlay();
	SendToyDropEvent();
	if(GetLocalRole() == ROLE_Authority && IsValid(this))
	{
		this->OnDestroyed.AddDynamic(this, &AT4PToyBase::SendOnDestroyedEvent);
	}
}



void AT4PToyBase::SendToyDropEvent()
{
	if(GetLocalRole() == ROLE_Authority)
	{
		AT4PGameStateBase* GS = Cast<AT4PGameStateBase>(GetWorld()->GetGameState());
        if (GS)
        {
        	GS->OnToyDrop(this);
        }
	}
}

void AT4PToyBase::SendToyHitEvent()
{
	if(GetLocalRole() == ROLE_Authority)
	{
		AT4PGameStateBase* GS = Cast<AT4PGameStateBase>(GetWorld()->GetGameState());
		if (GS)
		{
			GS->OnToyHit(this);
		}
	}
}


// Called every frame
void AT4PToyBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

